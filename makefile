SHELL := /bin/bash
mkfile_path := $(realpath $(lastword $(MAKEFILE_LIST)))
fake_home := $(realpath $(dir ${mkfile_path}))
gen_snippet_file := examples/snippets.yaml

.PHONY: gen-snippets run clean all
all: gen-snippets

run: texpander.sh
	HOME=$(fake_home) \
	./texpander.sh

gen-snippets: $(snippet_file) gen-snippets.sh
	mkdir -p "$(fake_home)/.texpander"
	HOME=$(fake_home) \
	./gen-snippets.sh "$(fake_home)/$(gen_snippet_file)"

clean:
	rm -rf .cache .texpander

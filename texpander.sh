#!/bin/bash

# Source: https://stackoverflow.com/a/3352015
# Removes leading and trailing whitespace characters from a string
function trim() {
  local var="$*"
  # remove leading whitespace characters
  var="${var#"${var%%[![:space:]]*}"}"
  # remove trailing whitespace characters
  var="${var%"${var##*[![:space:]]}"}"   
  printf '%s' "$var"
}

# Given a user-written string via STDIN, prints out a version that escapes awk's
# special keywords so the string can be properly used in a sub() or gsub() call
# as though it were a string literal.
function escape_for_awk() {
  # Source, using awk for editing newlines: https://stackoverflow.com/a/14853319
  # Source, using sed to escape quotes: https://stackoverflow.com/a/42341860
  local with_newline="$(sed -e 's/\\/\\\\/g' \
                         | awk 1 ORS="\\\n" \
                         | sed -e 's/\x22/\\\x22/g' \
                         | sed -e 's/\x27/\\\x27/g')"
  echo "${with_newline%\\n}" # output without trailing newline
}

# Given the path to a snippet file and the current clipboard, returns semicolon-seperated list
# of awk commands that can be used to replace all fillins and text-modifying macros.
# INPUT:
# - $1 is the path to a chosen snippet file.
# - $2 is the contents of the current clipboard. Currently, only single-line clipboard is
#   content is supported.
# OUTPUT:
# - A semicolon seperated list of awk commands that will replace any supported text fillins
#   with their expansion. For example, if %filltext:name=File% is in the snippet file,
#   the after selecting that snippet, the user will also be prompted to for a "File".
#   When they type what "File" is, let's say they type "The file I want", this script will
#   print out:
#      sub(/%filltext:name=File(default=)?/, "The file I want");
#   which is an awk command that will substitute "%filltext:name=File%" with the text
#   "The file I want".
function create_fillin_subpat() {
  local path="$1"
  local basedir=$(dirname "${path}")
  local clipboard=$(head -n 1 <<< "$2") # only supports single-line clipboard content atm
  local subpat=''
  # Source: ravenx99's fillin fork - https://github.com/leehblue/texpander/pull/29
  # Determine if the text requires variable substitution
  if grep -q '%filltext' "$path";  then
    # variable pattern is either
    # "%filltext:name=VAR%"
    # "%filltext:name=VAR:default=DEFAULT%"
    while read var def; do
      local val=$(echo "$def" | dmenu -p "${var}:")
      local cval=$(echo "$val" | escape_for_awk) # clean val
      # build an awk action to replace the filltext
      subpat+="sub(/%filltext:name=${var//:/\\\\:}(:default=[^%]+)?%/, \"${cval}\"); "
    done < <(grep -E -o '%filltext:name=([^:]|\\:)+(:default=([^:]|\\:)+)?%' "$path" \
               | sed 's/%//g; s/\(name=\|default=\|filltext:\)//g; s/\([^\\]\):/\1 /g; s/\\:/:/g;' \
               | sort -u -k1,1) 
    # grep returns just the matched text
    # sed cleans up and returns a newline-seperated list of the form: varname [defaultval]
    #   (if not given a default val, the only line is the testvar)
    # sort removes duplicate names
    # End Source: ravenx99's fillin fork
  fi
  if grep -q '%clipboard' "$path"; then
    subpat+="gsub(/%clipboard/, \"${clipboard}\"); "
  fi
  if grep -q '%snippet' "$path"; then
    # variable pattern is "%snippet:SNIPPETNAME%"
    while read snippet_name; do
      if [ -f "${basedir}/${snippet_name}" ]; then
        local snippet_subpat=$(create_fillin_subpat "${basedir}/${snippet_name}" "$2")
        local snippet=$(cat "${basedir}/${snippet_name}" \
                            | awk "{ $snippet_subpat; print }" \
                            | escape_for_awk)
        subpat+="sub(/%snippet:${snippet_name//:/\\\\:}%/, \"${snippet}\"); "
      else zenity --error --text="Snippet not found:\n${snippet_name}"
      fi
    done < <(grep -E -o '%snippet:([^:]|\\:)+%' "$path" \
               | sed 's/%//g; s/\(snippet:\)//g; s/\([^\\]\):/\1 /g; s/\\:/:/g;' \
               | sort -u -k1,1)
  fi
  if grep -q '%script' "$path"; then
    # variable pattern is either:
    # "%script:name=SCRIPTNAME%"
    # "%script:name=SCRIPTNAME:args=ARGNAME1 ARGNAME2 ...%"
    local greppat='%script:name=(([^:]|\\:)+)(:args=([^:]|\\:)+)?%'
    while read script_name arg_names; do
      local args=( $(trim "$arg_names") )
      if [ -f "${basedir}/${script_name}" ]; then
        for (( i=0; i<"${#args[@]}"; i++ )); do
          args[i]=$(echo "" | dmenu -p "${args[$i]}")
        done
        local out=$(sh ${basedir}/${script_name//:/\\:} "${args[@]}" 2>&1 | escape_for_awk)
        subpat+="sub(/%script:name=${script_name//:/\\\\:}(:args=${arg_names//:/\\\\:})?%/, "`
                     `"\"${out}\"); "
      else
        zenity --error --text="Snippet not found:\n$script_name"
        local errmsg="ERR: snippet \\'${script_name}\\' not found"
        subpat+="sub(/%script:name=${script_name//:/\\\\:}(:args=${arg_names//:/\\\\:})?%/,"`
                     `" \"[${errmsg}]\"); "
      fi
    done < <(grep -E -i "$greppat" "$path" \
               | sed 's/%//g; s/script://; s/name=//; 
                      s/\(.*\):args=\(.*\)/\1 \2/; 
                      s/\([^\\]\):/\1 /g; s/\\:/:/g;' \
               | sort -u -k1,1)
  fi
  echo "$subpat"
}

# When given text to its STDIN and a macro file, echoes text without any macros in it, and
# echoes a macro_queue to the tempfile. The macro file, after running, will be a newline-seperated
# list of macros, along with where they were found in the text, and their arguments.
# The specific format of each line in the file will be:
#   TEXT_INDEX MACRO_TYPE MACRO_ARGS
# The position in the list will be ordered by TEXT_INDEX and "macro precedence", which
# is listed from high-precedence (executed first) to low precedence (executed last) below:
# - key [KEY] : %key:<KEY>%
# - cursor    : %|
# - highlight : %\
# 
# Example:
# Snippet text is: "Name:MyriaCore%key:tab%Date:5/25%key:tab%Fav Color: %|Purple"
# Output will be: "Name:MyriaCoreDate:5/25Fav Color: Purple"
# Contents of macro_file will be:
#  | 14 key tab
#  | 23 key tab
#  | 34 cursor
function make_macro_file() {
  local text="$(cat)"
  local subpat=""
  local macro_file="$1" # A macro file provided to us
  local retval=0

  # Emulates a do-while loop
  # Source: https://stackoverflow.com/a/16489942
  while : ; do
    read idx macro args < <(echo "$text" | grep -ob '%key:[^%]*%\|%|\|%\\' \
                              | head -n 1 | sed 's/:/ /' \
                              | sed 's/%key:\([^%]*\)%/key \1/;' \
                              | sed 's/%|/cursor/' \
                              | sed 's/%\\/highlight/')

    # Break out if we didn't get anything back from read
    [ -n "$idx" ] || [ -n "$macro" ] || [ -n "$args" ] || break
    
    # Add the index, macro, and args (if there are any)
    [ -z "$args" ] && echo "$idx $macro" >> "$macro_file"
    [ ! -z "$args" ] && echo "$idx $macro $args" >> "$macro_file"
    
    # Reset text to remove the macro we just saw
    case "$macro" in
      'key') text="$(echo "$text" | sed 's/%key:[^%]*%//')"
             ( [ -n $seen_cursor ] || [ -n $seen_highlight ] ) && let 'retval = 1 | retval';;
      'cursor')
        seen_cursor=true
        text="$(echo "$text" | sed 's/%|//')";;
      'highlight')
        seen_highlight=true
        text="$(echo "$text" | sed 's/%\\//')";;
    esac
  done

  echo "$text"

  return $retval
}

# Given a path to a file or directory via $1, returns 0 if the path was valid,
# and won't cause any issues, and returns 1 if the path wasn't valid, and
# could potentially cause issues. 
function valid_snippet_name() { [[ $1 != *" [CATEGORY]"* ]]; }

# Given a path to a category of snippets via $1, returns 0 if the path to all
# if its snippets are valid and won't cause any issues, and returns 1
# if the path isn't valid, and could potentially cause issues.
function valid_category_name() {
  local results=$(find "$1" -name "* [CATEGORY]")
  [[ $results == "" ]]
}

# Asks the user to select a snippet, prints the path to the snippet that was selected.
# Allows for recursive directory traversal.
# INPUT:
# - $1 is the category/directory to select the snippet from. Cannot be higher than
#   $basedir.
# OUTPUT:
# - Prints the full path to the snippet that was selected. 
function select_snippet() {  
  # Set the prompt and flag appropriately
  [ "${base_dir}" != "$1" ] && local prompt="Choose Snippet[$(basename ${1})]" \
    && local highest_dir=false
  [ "${base_dir}" = "$1" ] && local prompt='Choose Snippet' && local highest_dir=true
  
  # Construct the array of snippets
  [ "$highest_dir" = true ] && \
    declare snippets=$(find "${1}" -maxdepth 1 \
                            -not -name "\.*" \
                            -not -path "$base_dir" \
                            -printf "%y %f\n" \
                         | sed -rn 's/^d (.*)$/\1 [CATEGORY]/p;  s/^f (.*)$/\1/p;  s/^[^df] //p;' \
                         | sort)
  [ "$highest_dir" = false ] && \
    declare snippets=$(find "${1}" -maxdepth 1 \
                            -not -name "\.*" \
                            -not -path "${1}" \
                            -printf "%y %f\n" \
                         | sed -rn 's/^d (.*)$/\1 [CATEGORY]/p;  s/^f (.*)$/\1/p;  s/^[^df] //p;' \
                         | awk '{ print $0 } END { print "Up [CATEGORY]" }' \
                         | sort)
  
  # Source: https://askubuntu.com/a/728914
  # Prompt the user to select a snippet in the directory given by $1
  local selection=$(printf '%s\n' "${snippets[@]}" | dmenu -l 20 -p "$prompt")

  # Process what the user just selected
  if [[ $selection == "" ]]; then                 # Error Case
    return 1
  elif [[ $selection == "Up [CATEGORY]" ]]; then  # Recursive Case: select in the parent dir
    select_snippet "$(realpath "${1}/..")"
  elif [[ $selection == *" [CATEGORY]" ]]; then   # Recursive Case: select in a user-specified dir
    select_snippet "$(realpath "${1}/${selection% \[CATEGORY\]}")"
  else                                            # Base Case
    echo "$(realpath "${1}/$selection")"
  fi

  return 0
}

# Inserts the the text given by $1 into the focused textbox by loading it into the clipboard
# and using `xdotool` to paste. 
function insert_text() {
  # Do nothing if the input string was empty
  [ -z "$1" ] && return 0
  
  # Source: ravenx99's fillin fork
  
  # Put text in primary buffer for Shift+Insert pasting
  echo -n "$1" | xsel -p -i
  
  # Put text in clipboard selection for apps like Firefox that 
  # insist on using the clipboard for all pasting
  echo -n "$1" |  xsel -b -i

  # Paste $text into current active window
  sleep 0.2
  xdotool key shift+Insert
  
  # End Source: ravenx99's fillin fork

  # If you're having trouble pasting into apps, use xdotool
  # to type into the app instead. This is a little bit slower
  # but may work better with some applications.
  #
  # Make xdotool type RETURN instead of LINEFEED characters 
  # otherwise some apps like Gmail in Firefox won't recognize
  # newline characters.
  #
  # To use this, comment out line #32 (xdotool key shift+Insert)
  # and uncomment the line below.
  #xdotool type -- "$(xsel -bo | tr \\n \\r | sed s/\\r*\$//)"
}

####### MAIN LOGIC ########

# Get window id, pass to getwindow pid to output the pid of current window
pid=$(xdotool getwindowfocus getwindowpid)

# Store text name of process based on pid of current window
proc_name=$(cat /proc/$pid/comm)

# If ~/.texpander directory does not exist, create it
if [ ! -d ${HOME}/.texpander ]; then
  mkdir ${HOME}/.texpander
fi

# Store base directory path, expand complete path using HOME environment variable
base_dir=$(realpath "${HOME}/.texpander")

# Validate Snippets & error out if necessary
# Source[concatenation of string literals]: https://stackoverflow.com/a/7729087
if ! valid_category_name "$base_dir"; then
  zenity --error --width=500 --height=25 \
         --text="One of your categories or snippets has an invalid name! \n"`
                `"Please ensure that none of them end with <tt> [CATEGORY]</tt>"
  exit 1
fi

path="$(select_snippet ${base_dir})"

[ -z "$path" ] && exit 0 # user escaped out of the dmenu, didn't select anything
if [ -f "$path" ]; then
  # Preserve the current value of the clipboard
  clipboard="$(xsel -b -o)"
  
  # will create a subpat variable for us, and will set `macro_queue` if
  # the snippet contains macros.
  subpat="$(create_fillin_subpat "$path" "$clipboard")"

  # Create a temp file to give to `make_macro_file`
  macro_file=$(mktemp)
  
  # Perform subpat substitution and macro generation with awk
  if ! text=$(echo -n "$(cat "$path")" \
                | awk -v RS='^$' "{ ${subpat} printf \"%s\", \$0}" \
                | make_macro_file "$macro_file")
  then invalid_macros=true
       zenity --error --width=500 --height=25 \
              --text="The snippet you tried to insert has an invalid use of macros! \n"`
                `"Please ensure that <tt>%|</tt> or <tt>%\\</tt> never occur before <tt>%key</tt>."
  fi

  # Create macro queue and get rid of the temporary macro file
  readarray -t macro_queue < "$macro_file"
  rm "$macro_file"

  # Insert text if there aren't macros or the macros are invalid. Process macros otherwise.
  if [ -z "${macro_queue[*]}" ] || [ -n "$invalid_macros" ]; then insert_text "$text"
  else
    prev_idx=0 # Starting index, to help us keep track of where we are
    # Loop through the elements in `macro_queue`
    for (( i=0; i<"${#macro_queue[@]}"; i++ )); do
      read idx macro macro_args < <(echo -n "${macro_queue[$i]}")
      let slice_len=idx-prev_idx

      # Insert the text from where we left off to where our macro starts
      insert_text "${text:$prev_idx:$slice_len}"

      # execute the macro given by `macro macro_args`
      case "$macro" in
        'key') xdotool key "$macro_args";;
        'cursor') # cursor_pos is where this macro wants us to move the mouse to
          let cursor_pos=idx;; 
        'highlight') # highlight_end_pos is where this macro wants us to move the highlighter to
          let highlight_end_pos=idx;;
      esac
      
      let prev_idx=idx
    done

    # Insert the rest of the text
    insert_text "${text:$prev_idx}"

    # pos is current mouse position after inserting this last bit of text
    pos="$(expr length "${text:$prev_idx}")" # Source: https://stackoverflow.com/a/46592387
    let pos=prev_idx+pos
    
    # gotta move the cursor
    [ -n "$cursor_pos" ] && {
      let distance=pos-cursor_pos
      for (( i=0; i<distance; i++ )); do xdotool key Left; done
      let pos=cursor_pos
    }

    # gotta highlight until highlight_end_pos
    [ -n "$highlight_end_pos" ] && {
      let distance=pos-highlight_end_pos
      xdotool keydown Shift
      for (( i=0; i<"${distance#-}"; i++ )); do # Source[abs val]: https://stackoverflow.com/a/47240327
        (( distance < 0 )) && xdotool key Right
        (( distance > 0 )) && xdotool key Left
        (( distance == 0 )) && break
      done
      xdotool keyup Shift
    }
  fi


  # Restore the original value of the clipboard
  sleep 0.5
  echo "$clipboard" | xsel -b -i

  exit 0
else
  zenity --error --text="Snippet not found:\n${name}"
  exit 1
fi


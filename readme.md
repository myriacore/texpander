# Texpander

Texpander is a simple text expander for Linux. It is sort of like Autokey, except it works off of text files that you put in your `~/.texpander` directory. Texpander is a bash script that uses xclip, xdotool, and zenity to let you type an abbreviation for something and it expands to whatever you have in the matching text file.

## Installation

`texpander.sh` requires the following tools, so make sure you install them:

- `xdotool`
- `zenity`
- `dmenu`
- `xsel`

`generate-snippets.sh` also relies on:

- [`yq`](https://github.com/kislyuk/yq)
- `jq`

I recommend installing those too, but it's only important if you plan on using `generate-snippets.sh`, which you don't need to use for `texpander.sh` to work.

Once you've done that, go ahead and setup texpander:

1. Put `texpander.sh` and `generate-snippets.sh` somewhere in your system (I have mine in hidden folders in my `~/.texpander` directory).
1. Create a keyboard shortcut that calls `texpander.sh`. This will be the keybinding that does the expansion, so make it nice and accessible! Mine is <kbd>Super+.</kbd>, but [Lee Blue](https://github.com/leehblue), the original author of this fork, used <kbd>Ctrl+Space</kbd> as his, which is probably a better spot for it.
1. Create a `~/.texpander` directory where you'll store your snippet files
1. Add some snippets to your `~/.texpander` directory, and you're set!

## Usage

### Adding Snippets

Texpander will look for snippet files in the `~/.texpander` directory, so just put your snippet files in there. When you run texpander, it'll give you a list of the snippet files and snippet categories it can see in `~/.texpander`, and ask you to choose one. Once you do, it'll insert the content of that file into the window you're looking at!

You *can* add these snippet files yourself, but if you're like me, and have a LOT of short, often one-line snippets, this is gonna be way too slow. That's where `gen-snippets.sh` comes in!

#### Using `gen-snippets.sh` to create snippet files for you

`gen-snippets.sh` will allow you to write your snippets as a JSON or YAML file, instead of creating a ton of individual snippet files yourself. I personally prefer YAML, and I recommend you use it over JSON, because multi-line strings are [difficult to represent](https://stackoverflow.com/a/16690236) in JSON. However, if you do want to use it anyways, the support is there. Here's an [example](examples/snippets.yaml) gen-snippets file:

```yaml
shrug: ¯\_(ツ)_/¯
upset: ಠ_ಠ
angry: ಠ╭╮ಠ
cannot unsee: ◉_◉
kbd: <kbd>%filltext:name=Key:default=Ctrl%</kbd>
now playing: |2-
  ɴᴏᴡ ᴘʟᴀʏɪɴɢ: %filltext:name=Song% (Feat: %filltext:name=Featured Artist%)
  ​        ───────────⚪──────  
  ◄◄⠀▐▐ ⠀►►5:12/ 7:𝟻𝟼 ───○ 🔊⠀ ᴴᴰ ⚙️
details: |2-
  <details>
  <summary>%filltext:name=Summary:default=View Dropdown%</summary>
  
  
  </details>
calc: "%script:name=.calc_script:args=calculator%"
.calc_script: |2-
  #!/bin/bash
  echo "$*" | bc
```

Here, I've created the snippets `shrug`, `upset`, `angry`, `cannot unsee`, `now playing`, `kbd`, `details`. The snippets' names go on the left, and their content goes on the right. For snippets that need to take multiple lines, like `details` and `now playing`, I can use `|2-`, and then use two spaces as indentation. Another thing to note about YAML is that you can use quotes around your snippets to avoid getting errors about illegal tokens. For example, in `calc`, we've started the snippet off with a `%` sign (because we wanna make use of the calculator script), but if we try to run `gen-snippets.sh` on this, it'll tell is that we can't start tokens with a `%` sign. To let YAML know that this isn't a toke, but a *string*, we can just wrap the whole thing in quotes.

To actually generate the snippets in the `~/.texpander` directory, we can just do:

```
$ ./gen-snippets.sh examples/snippets.yaml
Reading from YAML file snippets.yaml...
shrug -> ¯\_(ツ)_/¯
upset -> ಠ_ಠ
angry -> ಠ╭╮ಠ
cannot unsee -> ◉_◉
kbd -> <kbd>%filltext:name=Key:default=Ctrl%</kbd>
now playing -> 
 | ɴᴏᴡ ᴘʟᴀʏɪɴɢ: %filltext:name=Song% (Feat: %filltext:name=Featured Artist%)
 | ​        ───────────⚪──────  
 | ◄◄⠀▐▐ ⠀►►5:12/ 7:𝟻𝟼 ───○ 🔊⠀ ᴴᴰ ⚙️
details -> 
 | <details>
 | <summary>%filltext:name=Summary:default=View Dropdown%</summary>
 | 
 | 
 | </details>
calc -> %script:name=.calc_script:args=calculator%
.calc_script -> 
 | #!/bin/bash
 | echo "$*" | bc

Removing Current Entries...
Adding entries from snippets.yaml...

✅ Done! See ~/.texpander to find your new snippets!
```

And now, if we check our `~/.texpander` directory, we'll see that `gen-snippets.sh` has created some files for us:

```
$ ls -a ~/.texpander
 .   ..   angry   calc   .calc_script  'cannot unsee'   details   kbd  'now playing'   shrug   upset
$ cat ~/.texpander/angry
ಠ╭╮ಠ
```

As you can see, `gen-snippets.sh` has just created these snippet files for us! Be aware that it *does* delete all of the current snippets in `~/.texpander` before adding the ones specified in the file.

#### Snippet Categories

If you put any folders in your `~/.texpander` directory, texpander will recognize these as *snippet categories*. When you select these in the Select Snippet menu, you'll be re-prompted with the snippets that are in the folder you selected. 


### Using the supported macros and fillins in your snippets

This fork extends [leehblue's original texpander script](https://github.com/leehblue/texpander/) with more advanced, power-user-oriented functionality - particularly the addition of macros and fillins. While this functionality tries to use the spec laid out by the TextExpander software so as to allow for portability, sometimes the syntax diverges.

In Texpander, a fillin is generally anything that just involves simple text substitution. A macro, on the other hand, involves something more interactive, like simulating the pressing of keys, moving the cursor, etc.

Below are tables describing the syntax for macros and fillins:

<!-- Fillin Table -->
<table>

<thead>
  <tr>
    <th>Fillin</th>
    <th>Usage</th>
  </tr>
</thead>

<tr><td>Filltext</td>
<td>

```
%filltext:name=<VAR NAME>%
%filltext:name=<VAR NAME>:default=<DEFAULT VALUE>%
```
</td></tr>

<tr><td>Script Expansion</td>
<td>

```
%script:name=<SCRIPT NAME>%
%script:name=<SCRIPT NAME>:args=[<ARG NAME 1> <ARG NAME 2> ... <ARG NAME N>]%
```
</td></tr>

<tr><td>Clipboard Expansion</td>
<td>

```
%clipboard
```
</td></tr>

<tr><td>Snippet Expansion</td>
<td>

```
%snippet:<SNIPPET NAME>%
```
</td></tr>
</table>

<!-- Macro Table -->

<table>

<thead>
  <tr>
    <th>Macro</th>
    <th>Usage</th>
  </tr>
</thead>

<tr><td>Key</td>
<td>

```
%key:<KEY NAME>%
```
</td>
</tr>

<tr><td>Cursor Position</td>
<td>

```
%|
```
</td></tr>

<tr><td>Highlight To Position</td>
<td>

```
%\
```
</td></tr>
</table>


There are a few things you should keep in mind when using these:

- The `%clipboard` fillin only supports text content at the moment, *and* only supports the first line of text content at the moment. 
- In areas where you can specify an identifier (like the name of the script, the name of a variable, etc), the `:` character is used as a delimiter. If you want to have `:` be a part of the name, you'll have to escape it so it's clear you're *not* doing something else with the fillin. For example, `%snippet:\:tada\:%` would refer to a snippet whose name is literally `:tada:`. This might be useful if you use emoji shorthand a lot, and you wanna keep your muscle memory for that, but you also want to be able to use your own shorthands.
- When using `%snippet` and `%script`, "Snippet name" and "Script name" should be written relative to `~/.texpander`. So, if you had a script called `.myscript.sh` that was located at `~/.texpander/.myscript.sh`, you can just say `%script:.myscript.sh%`. However, if it was *actually* located somewhere like `~/.texpander/.scripts/.myscript.sh`, you'd have to specify `%script:.scripts/.myscript.sh`. 
- The "Select Snippet" menu doesn't list any hidden files, so if you have named any of your snippets with a dot in front, it won't show up. This is useful if you have a ton of snippets that use `%script`, but don't want to have a menu that's all cluttered up with them, since you'll never *really* be using them directly (at least until #19 gets implemented) 

#### Using the `%filltext` fillin

The `%filltext` fillin allows snippets to prompt for user-input, which will then expand into whatever text the user provided. 

For example, let's say we had the following snippet, called `kbd`:

```
<kbd>%filltext:key%</kbd>
```

After the user chooses `kbd` in the Choose Snippet menu, a popup will appear requesting the user enter the input for *key*. If the user types, say, `Ctrl`, the snippet will expand to:

```
<kbd>Ctrl</kbd>
```

This is very handy, because it let's us easily do things like specify keyboard inputs in places that don't have the happy `[[kbd]]` syntax.

#### Using the `%script` fillin:

The `%script` fillin allows us to send user-provided arguments to a shell script, and expand to its output. First, a few important points:

- This shell script must be specified as a snippet in the `~/.texpander` directory
- It must have a shebang so `sh` can correctly interpret where to go to find the shell's interpreter

With these caveats out of the way, let's look at an example. Let's say we have the following two snippets:

<table>

<thead>
<tr>
  <th>calendar</th>
  <th>calendar_bin.sh</th>
</tr>
</thead>

<tr>


<td>

```
%script:name=calendar_bin.sh%
```

</td>

<td>

```bash
#!/bin/bash
cal
```
</td>


</tr>
<table>

Our script here isn't too complicated - it just calls `cal`. So, when the user selects `calendar` from the Choose Snippet menu, the `calendar` snippet expands to the ouput of ours script, which just calls `cal`. This gives them a nice textual calendar!

When arguments are provided, the user will be prompeted for a value for the argument via a `%filltext`-style dialogue.

For example, let's say we have the following snippets:

<table>

<thead>
<tr>
  <th>calc</th>
  <th>calc_bin.sh</th>
</tr>
</thead>

<tr>


<td>

```
%script:name=calc_bin.sh:args=math%
```

</td>

<td>

```bash
#!/bin/bash
echo "$1" | bc
```
</td>


</tr>
<table>

Once again, our script isn't too complicated - we just pipe the user's input to `bc`, a simple command line utility for simple calculations (literally stands for 'basic calculator'). 

When the user selects the `calc` snippet, they'll be prompted to provide a value for the "math" variable. If the user types `1+4`, the snippet will expand to the output the following shell query:

```
$ sh ~/.texpander/calc_bin.sh "1+4"
```

... which will just pipe `"1+4"` to `bc`, which will give us `5`. As we can see, this is an incredibly powerful feature, as it allows for aribitrary transformation of the provided text. 


#### Using the `%key` macro

The `$key:<KEY NAME>%` macro can simulate at precise locations in the text of a snippet. They're pretty useful for things like form fills. 

Let's say you had the following form:

```plantuml
@startsalt
{
  Login    | "         "
  Password | "         "
  [Cancel] | [  OK   ]
}
@endsalt
```

And you wanted to make a snippet to enter `MyName` as the username, `MyPass` as the password, and select the `OK` button automatically. That snippet might look something like the following:

```
MyName%key:Tab%MyPass%key:Tab%%key:Tab%%key:Tab%
```

This macro will use the <kbd>Tab</kbd> key to tab over to select the different elements of the form fill. The result would of course fill in the following:

```plantuml
@startsalt
{
  Login    | "MyName   "
  Password | "****     "
  [Cancel] | {+[  OK   ]}
}
@endsalt
```

An important note here is that this macro should *never* occur after either the cursor or the highlight macro (`|%` and `%\` respectively). See [this](https://gitlab.com/myriacore/texpander/-/merge_requests/1#note_351330679) thread details about why this is. 

#### Using the `%|` and `%\` macros

The `%|` macro is the easiest to understand - it positions the cursor wherever it appeared after inserting the text. 

The `%\` macro is a big harder to understand. It makes the cursor highlight from wherever it previously was, to where the `%\` was found in the text. 

For example, let's say the user wanted allow for a `%fillin`-like experience, but wanted it to be a bit snappier. Instead of actually using `%fillin`, they could just use the default value, and wrap it with `%|` and `%\`. This way, if the user wanted to write their own thing, they could just delete the highlighted default value and write whatever they wanted. Let's see how this might look with the `kbd` snippet we defined [earlier](#using-gen-snippetssh-to-create-snippet-files-for-you):

<table>
<thead>
  <tr>
    <th>With <code>%fillin</code></th>
    <th>With <code>%|</code> and <code>%\</code></th>
  </tr>
</thead>
<tr>
<td>

```
<kbd>%filltext:name=Key:default=Ctrl%</kbd>
```
</td>

<td>

```
<kbd>%|Ctrl%\</kbd>
```
</td>
</tr>
</table>

## Contributing

1. [Create a Merge Request](https://gitlab.com/myriacore/texpander/-/merge_requests/new)
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a Merge Request :D

I've supplied a makefile and some [example gen-snippet files](examples) for you, which should make it easy to test things without messing up your current setup. 

- Both `make` and `make gen-snippets` will create a `.texpander` directory in the current directory, and will fill it with snippets from the [example snippets yaml file](examples/snippets.yaml). If you wanna use JSON instead, you can, but you'll just have to change the makefile's [`gen_snippet_file`](makefile#L4) variable to point to the [example gen-snippets json file](examples/snippets.json) instead.
- `make run` will run texpander with the current directory's `.texpander` directory (instead of your `~/.texpander`)

## Credits

- Forked from [Lee Blue's](https://github.com/leehblue/)  [texpander repository](https://github.com/leehblue/texpander/)
- Merged code from [Carl Cravens'](https://github.com/ravenx99) [variable substitution fork](https://github.com/leehblue/texpander/pull/29)

## License

General Public License v3.0

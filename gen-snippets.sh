#!/bin/bash

replacement_file=~/.texpander/.snippets.yaml
yaml_pat=".*\.(yaml|yml)"
json_pat=".*\.json"

# Source: https://stackoverflow.com/a/3352015
# Removes leading and trailing whitespace characters from a string
function trim() {
  local var="$*"
  # remove leading whitespace characters
  var="${var#"${var%%[![:space:]]*}"}"
  # remove trailing whitespace characters
  var="${var%"${var##*[![:space:]]}"}"
  printf '%s' "$var"
}

# Source: https://stackoverflow.com/a/39983421
function program_exists() {
  command -v $1 &> /dev/null
}

# $1 is the key
# $2 is the value
function print_keyval() {
  local key="$1"
  local val="$2"
  
  echo -e -n "$(trim $key) -> "
  # Source[tr]: https://unix.stackexchange.com/a/254645
  if (( $(echo -n "$val" | wc -l) == 0 )) # There's only one line in the value
  then echo "$val"
  else echo "" && echo "$val" | sed 's/^/ | /g'; fi
}

# Error Case: More than 2 args
if [[ $# > 1 ]]; then
  echo "USAGE: $0 [[json or yaml file]]"
  exit 1
elif [[ $# = 1 ]] && [ $1 == '-h' ] || [ $1 == '--help' ]; then
  echo -e "USAGE: $0 [[json or yaml file]]\n\n" \
       " Removes everything in the ~/.texpander directory, and populates it\n" \
       " with the snippets from the provided json or yaml file.\n\n" \
       " If no command line arguments are provided, $0 will attempt to use the\n" \
       " file at ~/.texpander/.snippets.yaml instead.\n"
  exit 0
fi

# Check replacement_file or reassign to a provided one.
if [ -r "$1" ]; then
  replacement_file="$1"
fi

# Source, Regex Match: https://stackoverflow.com/a/17421041
# Error Case: appropriate parser (jq or yq) isn't installed
if [[ $replacement_file =~ $yaml_pat ]] && ! program_exists yq; then
  echo "ERROR: yq must be installed for yaml replacement files to work."
  exit 1
elif [[ $replacement_file =~ $json_pat ]] && ! program_exists jq; then
  echo "ERROR: jq must be installed for json replacement files to work."
  exit 1
fi

declare -A texreplacements
# check to see if it's a yaml or json, parse accordingly
if [[ $replacement_file =~ $yaml_pat ]]; then
  echo "Reading from YAML file $(basename ${replacement_file})..."
  if ! entries="$(yq -r 'to_entries
                         | map("texreplacements[\(.key|@sh)]=\(.value|@sh)")
                         |.[]' $replacement_file)"
  then echo -e "\n❌ ERROR: yq failed with error code $#."; exit 1
  else eval "$entries"; fi
elif [[ $replacement_file =~ $json_pat ]]; then
  echo "Reading from JSON file $(basename ${replacement_file})..."
  if ! entries="$(jq -r 'to_entries
                         | map("texreplacements[\(.key|@sh)]=\(.value|@sh)")
                         |.[]' $replacement_file)"
  then echo -e "\n❌ ERROR: jq failed with error code $#."; exit 1
  else eval "$entries"; fi
else
  echo "ERROR: Replacement file must be a json or yaml document."
  exit 1
fi

cd ~/.texpander/
echo -e "\nRemoving Current Entries..."
rm -rf ~/.texpander/**

echo "Adding entries from $(basename ${replacement_file})..."
for key in "${!texreplacements[@]}"; do
  print_keyval "$key" "${texreplacements[$key]}"
  echo "${texreplacements[$key]}" > "$key"
done

echo -e "\n✅ Done! See ~/.texpander to find your new snippets!"
exit 0
